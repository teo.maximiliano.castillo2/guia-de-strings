/*1) Permitir que el usuario ingrese una
palabra de hasta 15 letras. Mostrar en
pantalla cuántas letras tiene la palabra
que ingresó. (Por ejemplo, si ingresé 
"hola" debe verse en pantalla "4")*/
#include <stdio.h>
#include<conio.h>
#define MAX_LETRAS 16

int main (){
    char nombre[MAX_LETRAS];
    int cont_letras=0, i;

    printf("\nIngrese una palabra de 15 letras o menos: ");
    scanf("%s", nombre);

    for(i = 0; i < MAX_LETRAS && nombre[i] != 0; i++)
    {
        cont_letras++;
    }
    printf("\nSe ingresaron %d letras", cont_letras);
    getch();
}