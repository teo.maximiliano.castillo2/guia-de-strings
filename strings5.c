/*5) El usuario ingresa una palabra.
Mostrar en pantalla cuántas vocales
minúsculas y mayúsculas contiene.*/
#include <stdio.h>
#include<conio.h>
#define MAX_LETRAS 20

int main (){
    char palabra[MAX_LETRAS];
    int cont_voc_min = 0;   //cuanta vocales minusculas
    int cont_voc_may = 0;   //cuanta vocales mayusculas
    int i;

    printf("\nIngrese una palabra de 20 letras o menos: ");
    scanf("%s", palabra);

    for (i = 0; i < MAX_LETRAS && palabra[i] != 0; i++)
    {
        if (palabra[i] == 'a' || palabra[i] == 'e' || palabra[i] == 'i' || palabra[i] == 'o' || palabra[i] == 'u')    
            cont_voc_min++;

        if (palabra[i] == 'A' || palabra[i] == 'E' || palabra[i] == 'I' || palabra[i] == 'O' || palabra[i] == 'U')    
            cont_voc_may++;
    }
    
    printf("\nSe ingresaron %d vocales minusculas y %d vocales mayusculas", cont_voc_min, cont_voc_may);
    getch();
}
