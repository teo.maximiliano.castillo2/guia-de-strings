/*7) El usuario ingresará 5 nombres de personas
y sus edades (número entero). Luego de finalizar
el ingreso, muestre en pantalla el nombre de la
persona más joven. El ingreso se realiza de este
modo: nombre y edad de la primera persona, luego
nombre y edad de la segunda, etc...
Nota: no hay que almacenar todos los nombres y todas las notas.*/
#include <stdio.h>
#include<conio.h>
#include<string.h>
#define MAX_LETRAS 20
#define MAX_PERSONAS 5

int main (){
    char nombre[MAX_LETRAS];    //palabra que ingresa el usuario
    char nombre_min[MAX_LETRAS];//nombre de persona mas joven
    int edad[MAX_PERSONAS];
    int edad_min = 1000;
    int i;
    
    for (i = 0; i < MAX_PERSONAS; i++)
    {
        printf("\nIngrese un nombre: ");
        scanf("%s",nombre);
        printf("\nIngrese la edad de la persona: ");
        scanf("%d",&edad[i]);
        if (edad[i] <= edad_min)
        {
            edad_min = edad[i];
            strcpy(nombre_min, nombre);
        }
    }
    
    printf("\nLa persona mas joven es %s",nombre_min);
    getch();
}
