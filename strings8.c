/*8) El usuario ingresará nombres de personas
hasta que ingrese la palabra "FIN". No sabemos
cuántos nombres ingresará. Luego de finalizar
el ingreso, mostrar en pantalla cuál es el primer
nombre en orden alfabético de todos los ingresados.*/
#include <stdio.h>
#include<conio.h>
#include<string.h>
#define MAX_LETRAS 100

int main (){
    char nombre[MAX_LETRAS];        //nombre que ingresa el usuario
    char primer_nombre[MAX_LETRAS]; //primer nombre de persona en orden alfabetico
    
    printf("\nIngrese un nombre o la palabra FIN para finalizar el programa: ");
    scanf("%s", nombre);
    strcpy(primer_nombre, nombre);
    if (strcmp(nombre, "FIN") == 0)    return 0;
    
    while (strcmp(nombre, "FIN") != 0)
    {
        printf("\nIngrese un nombre o la palabra FIN para finalizar el programa: ");
        scanf("%s", nombre);
        if (strcmp(nombre,primer_nombre) < 0)    strcpy(primer_nombre, nombre);
        
    }

    printf("\nEl primer nombre en orden alfabetico es: %s", primer_nombre);
    getch();
    return 0;
}
