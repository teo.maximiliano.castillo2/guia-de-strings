/*2) Al punto 1, agregue que se 
muestre en pantalla la palabra
ingresada pero al revés. Si ingresé
"hola" debe verse "aloh".Pruebe con
varias palabras para verificar el 
funcionamiento (siempre menos de 15 letras).*/
#include <stdio.h>
#include<conio.h>
#define MAX_LETRAS 16

int main (){
    char nombre[MAX_LETRAS];
    int cont_letras=0;
    int i;

    printf("\nIngrese una palabra de 15 letras o menos: ");
    scanf("%s", nombre);
    for(i = 0; i < MAX_LETRAS && nombre[i] != 0; i++)
    {
        cont_letras++;
    }

    printf("\nSe ingresaron %d letras", cont_letras);
    printf("\nLa palabra al reves es:");
    for ( i = cont_letras; i >= 0; i--)
    {
        printf("%c", nombre[i]);
    }

    getch();
}