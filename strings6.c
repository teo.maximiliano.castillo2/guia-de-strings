/*6) El usuario ingresa una palabra.
Determinar qué letra aparece mayor
cantidad de veces. Para simplificar
el problema, trabaje solo con mayúsculas.*/
#include <stdio.h>
#include<conio.h>
#define MAX_LETRAS 20

int main (){
    char palabra[MAX_LETRAS];     //palabra que ingresa el usuario
    char letras[MAX_LETRAS];      //guarda la posicion de las letras de la palabra
    char cont_letras[MAX_LETRAS]; //cuanta la cantidad de letras que hay segun la posicion de cada una
    int may_letra = 0;            //guarda letra que mas se repite
    int imprimir = -1;             //se usa para indicar que print se va a utilizar
    int i;
    int j;

    printf("\nIngrese una palabra de 20 letras o menos: ");
    scanf("%s", palabra);
    for (i = 0; i < MAX_LETRAS && palabra[i] != 0; i++)
    {
        letras[i] = palabra[i];
        cont_letras[i]=0;
    }

    for ( i = 0; i < MAX_LETRAS && palabra[i] != 0; i++)//cuenta cantidad de letras
    {
        for (j = 0; j < MAX_LETRAS && palabra[j] != 0; j++)
        {
            if (letras[i] == letras[j])    cont_letras[i]++;
        }
        
    }
    
    for ( i = 0; i < MAX_LETRAS && palabra[i] != 0; i++)//busca la letra mas repetida y la iguala a may_letra
    {
        for (j = 0; j < MAX_LETRAS && palabra[j] != 0; j++)
        {
            if (cont_letras[i] > cont_letras[j] && j != i)    may_letra = letras[i]; 
        }

    }

    for ( i = 0; i < MAX_LETRAS && palabra[i] != 0; i++)
    {
        if(cont_letras[i] != 1)    imprimir = 1;
    }
    
    if (imprimir == -1) printf("\nNo hay letra que se repita mas de una vez");
    
    else    printf("\nLa letra que mas se repitio es %c", may_letra);
    
    getch();
}