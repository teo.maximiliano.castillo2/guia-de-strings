/*4) El usuario ingresa una palabra.
Mostrar en pantalla cuántas letras 
A minúsculas contiene.*/
#include <stdio.h>
#include<conio.h>
#define MAX_LETRAS 20

int main (){
    char palabra[MAX_LETRAS];
    int cont_de_a = 0;
    int i;

    printf("\nIngrese una palabra de 20 letras o menos: ");
    scanf("%s", palabra);

    for (i = 0; i < MAX_LETRAS && palabra[i] != 0; i++)
    {
        if (palabra[i] == 'a')    cont_de_a++;
    }
    
    printf("\nSe ingresaron %d a minusculas", cont_de_a);
    getch();
}
