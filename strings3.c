/*3) Permita que el usuario escriba dos
palabras, haciendo dos scanf y obteniendo
dos strings. Muestre en pantalla si las
dos palabras ingresadas son iguales, o no.
Iguales quiere decir idénticas: mismas letras
en las mismas posiciones.*/
#include <stdio.h>
#include<conio.h>
#define MAX_LETRAS 16

int main (){
    char nombre1[MAX_LETRAS];
    char nombre2[MAX_LETRAS];
    int cont_iguales=0;
    int cont_letras=0;
    int i;

    printf("\nIngrese una palabra de 15 letras o menos: ");
    scanf("%s", nombre1);
    printf("\nIngrese una palabra de 15 letras o menos: ");
    scanf("%s", nombre2);
    for(i = 0; i < MAX_LETRAS && nombre1[i] != 0; i++)
    {
        cont_letras++;
    }

    for ( i = 0; i < cont_letras; i++)
    {
        if (nombre1[i] == nombre2[i])   cont_iguales++; 
    }
    
    if (cont_iguales == cont_letras)    printf("\nLas palabras son iguales");

    else printf("\nLas palabras no son iguales");

    getch();
}
