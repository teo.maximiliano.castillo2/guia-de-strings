/*9) El usuario ingresará una palabra de hasta 10 letras.
Se desea mostrarla en pantalla pero encriptada según el "Código César".
Esto consiste en reemplazar cada letra con la tercera
consecutiva en el abecedario. Por ejemplo, "CASA" se
convierte en "FDVD".Tener en cuenta que las últimas 
letras deben volver al inicio, por ejemplo la Y se convierte B.
Este mecanismo se utilizaba en el Imperio Romano.
*/
#include <stdio.h>
#include<conio.h>
#include<string.h>
#define MAX_LETRAS 10
#define MAX_PERSONAS 5

int main (){
    char palabra[MAX_LETRAS];    //palabra que ingresa el usuario
    char palabra_cesar[MAX_LETRAS];//palabra ingresada en pasada a codigo cesar
    int i;

    printf("\nIngrese una palabra: ");
    scanf("%s",palabra);
    for (i = 0; i < MAX_LETRAS && palabra[i] != 0; i++)
    {
        if (palabra[i] == 'x' || palabra[i] == 'y' || palabra[i] == 'z' || palabra[i] == 'X' || palabra[i] == 'Y' || palabra[i] == 'Z')
        {
            if (palabra[i] == 'x')    palabra[i] = 'a';

            if (palabra[i] == 'y')    palabra[i] = 'b';

            if (palabra[i] == 'z')    palabra[i] = 'c';

            if (palabra[i] == 'X')    palabra[i] = 'A';

            if (palabra[i] == 'Y')    palabra[i] = 'B';

            if (palabra[i] == 'Z')    palabra[i] = 'C';
        }
        else    palabra[i] = palabra[i] + 3;
        
    }
    
    printf("\nLa palabra en codigo cesar es: %s",palabra);
    getch();
}